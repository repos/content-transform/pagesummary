<?php
namespace MediaWiki\Extension\PageSummary;

use MediaWiki\Rest\HttpException;
use Wikimedia\Parsoid\DOM\Document;
use Wikimedia\Parsoid\Utils\DOMUtils;

/**
 * Page summary output
 */
class SummaryOutput {
	/** @var string */
	public $title;

	/** @var string */
	public $pageHtml;

	/** @var string[][] */
	public $pageProps;

	/** @var array */
	public $pageImages;

	/** @var int */
	public $revisionId;

	/** @var array|bool */
	public $pageCoordinates;

	/** @var bool */
	public $shouldReturnEmptyExtracts;

	/**
	 * @param string $title The title of the page to summarize
	 * @param string $pageHtml Parsoid access service
	 * @param string[][] $pageProps The properties of the page
	 * @param array $pageImages The images of the page
	 * @param int $revisionId The revision id of the page
	 * @param array|null $pageCoordinates The primary coordinates of the given page, if any
	 * @param bool $shouldReturnEmptyExtracts Determines if empty extracts should be returned for the page
	 */
	public function __construct(
		$title,
		$pageHtml,
		$pageProps,
		$pageImages,
		$revisionId,
		$pageCoordinates,
		$shouldReturnEmptyExtracts
	) {
		$this->title = $title;
		$this->pageHtml = $pageHtml;
		$this->pageProps = $pageProps;
		$this->pageImages = $pageImages;
		$this->revisionId = $revisionId;
		$this->pageCoordinates = $pageCoordinates;
		$this->shouldReturnEmptyExtracts = $shouldReturnEmptyExtracts;
	}

	public function generate() {
		$output = [
			"description" => "Description for page $this->title",
			"pageProps" => $this->pageProps,
			"pageImages" => $this->pageImages,
			"revisionId" => $this->revisionId,
			"shouldReturnEmptyExtracts" => $this->shouldReturnEmptyExtracts
		];
		if ( $this->pageCoordinates ) {
			$output["pageCoordinates"] = $this->pageCoordinates;
		}
		return $output;
	}

	/**
	 * Parse the parsoid HTML to extract the lead section
	 * @param string $html Parsoid HTML for the page
	 * @return Document A new Document containing only the lead section
	 */
	public function createDocumentFromLeadSection( $html ) {
		$modifiedHtml = preg_replace( [ "/<(\/?)section([^>]*)>/" ], [ "<$1div$2>" ], $html );
		preg_match( "/(<div[^>]*data-mw-section-id=\"0\"[^>]*>.*?<\/div>)/is", $modifiedHtml, $matches );
		$leadSection = isset( $matches[1] ) ? $matches[1] : "";
		if ( !isset( $matches[1] ) ) {
			throw new HttpException( "HTML input should be a string", 500 );
		}

		$domDocument = DOMUtils::parseHTML( $leadSection );
		$domDocument->loadHTML( $leadSection );
		return $domDocument;
	}

	/**
	 * Builds the extract values.
	 *
	 * @param string $html Parsoid HTML for the page
	 * @param array $processing The summary processing steps
	 * @return array The extract values
	 */
	public function buildExtractsFromHtml( $html, $processing ) {
		if ( $this->shouldReturnEmptyExtracts ) {
			return [ "extract" => "", "extract_html" => "" ];
		}
		$document = $this->createDocumentFromLeadSection( $html );

		// TODO: The return value below is a placeholder depending on buildExtractsFromHtml implementation.
		return [ "extractValues" => "testing1" ];
	}

}
