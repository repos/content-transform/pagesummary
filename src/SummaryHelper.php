<?php
namespace MediaWiki\Extension\PageSummary;

use GeoData\GeoData;
use MediaWiki\Page\ExistingPageRecord;
use MediaWiki\Page\PageProps;
use MediaWiki\Page\PageStore;
use MediaWiki\Permissions\Authority;
use MediaWiki\Rest\Handler\Helper\HtmlOutputRendererHelper;
use MediaWiki\Rest\Handler\Helper\PageRestHelperFactory;
use MediaWiki\Rest\HttpException;
use MediaWiki\Revision\RevisionLookup;
use MediaWiki\Title\TitleFactory;
use PageImages\PageImages;

/**
 * Page summary helper
 */
class SummaryHelper {
	/** @var string */
	public $title;

	/** @var int */
	public $nsMain;

	/** @var int[] */
	public $summaryNsAllowlist;

	/** @var PageStore */
	private $pageStore;

	/** @var PageProps */
	private $pageProps;

	/** @var RevisionLookup */
	private $revisionLookup;

	/** @var HtmlOutputRendererHelper */
	private $htmlHelper;

	/** @var Authority */
	private $authority;

	/** @var ExistingPageRecord */
	private $page;

	/** @var TitleFactory */
	private $titleFactory;

	/**
	 * @param string $title The title of the page to summarize
	 * @param ?Authority $authority Parsoid access service
	 * @param PageStore $pageStore Page store service
	 * @param PageRestHelperFactory $restHelperFactory Rest helper factory
	 * @param PageProps $pageProps Page props service
	 * @param RevisionLookup $revisionLookup Page revision service
	 * @param TitleFactory $titleFactory Title factory
	 */
	public function __construct(
		$title,
		$authority,
		$pageStore,
		$restHelperFactory,
		$pageProps,
		$revisionLookup,
		$titleFactory
	) {
		$this->title = $title;
		$this->nsMain = 0;
		$this->summaryNsAllowlist = [ $this->nsMain ];
		$this->authority = $authority;
		$this->pageStore = $pageStore;
		$this->htmlHelper = $restHelperFactory->newHtmlOutputRendererHelper();
		$this->pageProps = $pageProps;
		$this->revisionLookup = $revisionLookup;
		$this->titleFactory = $titleFactory;
		$this->page = $this->getPage();
	}

	/**
	 * Get page record
	 * @throws HttpException if page record doesn't exist
	 * @return ExistingPageRecord
	 */
	public function getPage() {
		$page = $this->pageStore->getExistingPageByText( $this->title );
		if ( !$page ) {
			throw new HttpException( "Page not found", 404 );
		}

		return $page;
	}

	/**
	 * Get page html
	 * @return string
	 */
	public function getPageHtml() {
		$this->htmlHelper->init( $this->getPage(), [], $this->authority, null );
		return $this->htmlHelper->getHtml()->getRawText();
	}

	/**
	 * Get page props
	 * @return string[][]
	 */
	public function getPageProps() {
		return $this->pageProps->getAllProperties( $this->getPage() );
	}

	/**
	 * @param int $size Max width in pixels of thumbnail images.
	 * @return array[]
	 */
	public function getPageImages( $size = 320 ) {
		$pageId = $this->page->getId();
		return PageImages::getImages( [ $pageId ], $size );
	}

	/**
	 * Get current revision id of page
	 * @throws HttpException if page record doesn't exist
	 * @return int
	 */
	public function getRevisionId() {
		$pageId = $this->page->getId();
		$revision = $this->revisionLookup->getRevisionByPageId( $pageId );
		if ( !$revision ) {
			throw new HttpException( "Revision not found", 404 );
		}

		return $revision->getId();
	}

	/**
	 * Returns primary coordinates of the given page, if any
	 * @return array|null Coordinates or null
	 */
	public function getPageCoordinates() {
		$pageId = $this->page->getId();
		$coordinates = GeoData::getPageCoordinates( $pageId );
		if ( $coordinates ) {
			return [ "lat" => $coordinates->lat, "lon" => $coordinates->lon ];
		}
		return null;
	}

	/**
	 * Check if empty extracts should be returned
	 * @return bool true if empty extracts should be returned, false otherwise
	 */
	public function shouldReturnEmptyExtracts() {
		$pageId = $this->page->getId();
		$namespace = $this->page->getNamespace();
		$titleInstance = $this->titleFactory->newFromID( $pageId );

		$notSupportedNS = !in_array( $namespace, $this->summaryNsAllowlist );
		$isRedirect = $this->page->isRedirect();
		$isWikitextPage = $titleInstance->isWikitextPage();
		$isMainPage = $titleInstance->isMainPage();

		if ( $notSupportedNS || $isRedirect || $isWikitextPage || $isMainPage ) {
			return true;
		}
		return false;
	}
}
