<?php
namespace MediaWiki\Extension\PageSummary;

use MediaWiki\MediaWikiServices;
use MediaWiki\Rest\Handler;
use MediaWiki\Rest\Response;
use MediaWiki\Rest\SimpleHandler;
use Wikimedia\ParamValidator\ParamValidator;

/**
 * Handler for displaying or rendering a page summary:
 * - /v1/page/summary/{title}
 */
class RestHandler extends SimpleHandler {
	public function needsWriteAccess() {
		return false;
	}

	/**
	 * Handle request for page summary
	 * @param string $title
	 * @return Response
	 */
	public function run( $title ) {
		$services = MediaWikiServices::getInstance();
		$summaryHelper = new SummaryHelper(
			$title,
			$this->getAuthority(),
			$services->getPageStore(),
			$services->getPageRestHelperFactory(),
			$services->getPageProps(),
			$services->getRevisionLookup(),
			$services->getTitleFactory(),
		);
		$summaryOutput = new SummaryOutput(
			$title,
			$summaryHelper->getPageHtml(),
			$summaryHelper->getPageProps(),
			$summaryHelper->getPageImages(),
			$summaryHelper->getRevisionId(),
			$summaryHelper->getPageCoordinates(),
			$summaryHelper->shouldReturnEmptyExtracts()
		);
		return $this->getResponseFactory()->createJson( $summaryOutput->generate() );
	}

	/**
	 * Extract parameters from URL route
	 * @return array[]
	 */
	public function getParamSettings() {
		return [
			"title" => [
				Handler::PARAM_SOURCE => "path",
				ParamValidator::PARAM_TYPE => "string",
				ParamValidator::PARAM_REQUIRED => true,
			]
		];
	}

}
