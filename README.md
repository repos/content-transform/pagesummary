# Summary extension
## Description
Prototyping work to port the nodejs implementation of `/page/summary` from PCS services to MW.

## Quick start
1. Install a local MediaWiki instance
2. Register the extension in your `LocalSettings.php`
   - `wfLoadExtension( "PageSummary" )`

Once the extension is installed in a MW instance the endpoint is registered in this path:
* `/w/rest.php/v1/summary/<title>`

To run the unit tests:
```
> cd /path/to/mediawiki
> composer phpunit:entrypoint -- extensions/PageSummary/tests/phpunit/unit
```

To run the linter:
```
> cd /path/to/extensions/pagesummary
> composer run test
> composer run fix
```
