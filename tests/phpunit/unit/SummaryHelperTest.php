<?php

use MediaWiki\Extension\PageSummary\SummaryHelper;
use MediaWiki\Page\ExistingPageRecord;
use MediaWiki\Page\PageProps;
use MediaWiki\Page\PageStore;
use MediaWiki\Parser\ParserOutput;
use MediaWiki\Rest\Handler\Helper\HtmlOutputRendererHelper;
use MediaWiki\Rest\Handler\Helper\PageRestHelperFactory;
use MediaWiki\Revision\RevisionLookup;
use MediaWiki\Revision\RevisionRecord;
use MediaWiki\Title\TitleFactory;
use MediaWiki\User\User;
use PHPUnit\Framework\TestCase;

/**
 * @covers MediaWiki\Extension\PageSummary\SummaryHelper
 */
final class SummaryHelperTest extends TestCase {
	public function testGetExistingPage(): void {
		$title = "Test title";
		$authority = null;
		$pageStoreMock = $this->getMockBuilder( PageStore::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock = $this->getMockBuilder( PageRestHelperFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pagePropsMock = $this->getMockBuilder( PageProps::class )
			->disableOriginalConstructor()
			->getMock();
		$revisionLookupMock = $this->getMockBuilder( RevisionLookup::class )
			->disableOriginalConstructor()
			->getMock();
		$htmlHelperMock = $this->getMockBuilder( HtmlOutputRendererHelper::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock
			->expects( $this->once() )
			->method( "newHtmlOutputRendererHelper" )
			->willReturn( $htmlHelperMock );
		$pageMock = $this->getMockBuilder( ExistingPageRecord::class )
			->disableOriginalConstructor()
			->getMock();
		$titleFactoryMock = $this->getMockBuilder( TitleFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pageStoreMock
			->expects( $this->exactly( 2 ) )
			->method( "getExistingPageByText" )
			->with( $title )
			->willReturn( $pageMock );
		$helper = new SummaryHelper(
			$title, $authority, $pageStoreMock, $restHelperFactoryMock, $pagePropsMock, $revisionLookupMock, $titleFactoryMock
		);
		$page = $helper->getPage();
		$this->assertEquals( $pageMock, $page );
	}

	public function testGetMissingPage(): void {
		$title = "Test title";
		$authority = null;
		$pageStoreMock = $this->getMockBuilder( PageStore::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock = $this->getMockBuilder( PageRestHelperFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pagePropsMock = $this->getMockBuilder( PageProps::class )
			->disableOriginalConstructor()
			->getMock();
		$revisionLookupMock = $this->getMockBuilder( RevisionLookup::class )
			->disableOriginalConstructor()
			->getMock();
		$htmlHelperMock = $this->getMockBuilder( HtmlOutputRendererHelper::class )
			->disableOriginalConstructor()
			->getMock();
		$titleFactoryMock = $this->getMockBuilder( TitleFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock
			->expects( $this->once() )
			->method( "newHtmlOutputRendererHelper" )
			->willReturn( $htmlHelperMock );
		$pageStoreMock
			->expects( $this->once() )
			->method( "getExistingPageByText" )
			->with( $title )
			->willReturn( null );

		$this->expectException( \MediaWiki\Rest\HttpException::class );
		new SummaryHelper(
			$title, $authority, $pageStoreMock, $restHelperFactoryMock, $pagePropsMock, $revisionLookupMock, $titleFactoryMock
		);
	}

	public function testGetPageHTML(): void {
		$expectedHTML = "<html><body><h1>Title</h1></body></html>";
		$title = "Test title";
		$authority = $this->getMockBuilder( User::class )
			->disableOriginalConstructor()
			->getMock();
		$pageStoreMock = $this->getMockBuilder( PageStore::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock = $this->getMockBuilder( PageRestHelperFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pagePropsMock = $this->getMockBuilder( PageProps::class )
			->disableOriginalConstructor()
			->getMock();
		$revisionLookupMock = $this->getMockBuilder( RevisionLookup::class )
			->disableOriginalConstructor()
			->getMock();
		$htmlHelperMock = $this->getMockBuilder( HtmlOutputRendererHelper::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock
			->expects( $this->once() )
			->method( "newHtmlOutputRendererHelper" )
			->willReturn( $htmlHelperMock );
		$pageMock = $this->getMockBuilder( ExistingPageRecord::class )
			->disableOriginalConstructor()
			->getMock();
		$pageStoreMock
			->expects( $this->exactly( 2 ) )
			->method( "getExistingPageByText" )
			->with( $title )
			->willReturn( $pageMock );
		$titleFactoryMock = $this->getMockBuilder( TitleFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$htmlHelperMock
			->expects( $this->once() )
			->method( "init" )
			->with( $pageMock, [], $authority, null );
		$parserOutputMock = $this->getMockBuilder( ParserOutput::class )
			->disableOriginalConstructor()
			->getMock();
		$parserOutputMock
			->expects( $this->once() )
			->method( "getRawText" )
			->willReturn( $expectedHTML );
		$htmlHelperMock
			->expects( $this->once() )
			->method( "getHtml" )
			->willReturn( $parserOutputMock );

		$helper = new SummaryHelper(
			$title, $authority, $pageStoreMock, $restHelperFactoryMock, $pagePropsMock, $revisionLookupMock, $titleFactoryMock
		);
		$html = $helper->getPageHtml();
		$this->assertEquals( $expectedHTML, $html );
	}

	public function testGetPageProps(): void {
		$title = "Test title";
		$authority = null;
		$pageStoreMock = $this->getMockBuilder( PageStore::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock = $this->getMockBuilder( PageRestHelperFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pagePropsMock = $this->getMockBuilder( PageProps::class )
			->disableOriginalConstructor()
			->getMock();
		$revisionLookupMock = $this->getMockBuilder( RevisionLookup::class )
			->disableOriginalConstructor()
			->getMock();
		$htmlHelperMock = $this->getMockBuilder( HtmlOutputRendererHelper::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock
			->expects( $this->once() )
			->method( "newHtmlOutputRendererHelper" )
			->willReturn( $htmlHelperMock );
		$pageMock = $this->getMockBuilder( ExistingPageRecord::class )
			->disableOriginalConstructor()
			->getMock();
		$titleFactoryMock = $this->getMockBuilder( TitleFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pageStoreMock
			->expects( $this->exactly( 2 ) )
			->method( "getExistingPageByText" )
			->with( $title )
			->willReturn( $pageMock );
		$expectedProps = [ "key1" => "value1", "key2" => "value2" ];
		$pagePropsMock
			->expects( $this->once() )
			->method( "getAllProperties" )
			->with( $pageMock )
			->willReturn( $expectedProps );

		$helper = new SummaryHelper(
			$title, $authority, $pageStoreMock, $restHelperFactoryMock, $pagePropsMock, $revisionLookupMock, $titleFactoryMock
		);
		$props = $helper->getPageProps();
		$this->assertEquals( $expectedProps, $props );
	}

	public function testGetRevisionIdSuccess(): void {
		$title = "Test title";
		$authority = null;
		$pageStoreMock = $this->getMockBuilder( PageStore::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock = $this->getMockBuilder( PageRestHelperFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pagePropsMock = $this->getMockBuilder( PageProps::class )
			->disableOriginalConstructor()
			->getMock();
		$revisionLookupMock = $this->getMockBuilder( RevisionLookup::class )
			->disableOriginalConstructor()
			->getMock();
		$htmlHelperMock = $this->getMockBuilder( HtmlOutputRendererHelper::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock
			->expects( $this->once() )
			->method( "newHtmlOutputRendererHelper" )
			->willReturn( $htmlHelperMock );
		$pageMock = $this->getMockBuilder( ExistingPageRecord::class )
			->disableOriginalConstructor()
			->getMock();
		$titleFactoryMock = $this->getMockBuilder( TitleFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pageStoreMock
			->expects( $this->once() )
			->method( "getExistingPageByText" )
			->with( $title )
			->willReturn( $pageMock );
		$expectedPageId = 11;
		$pageMock
			->expects( $this->once() )
			->method( "getId" )
			->willReturn( $expectedPageId );
		$revisionRecordMock = $this->getMockBuilder( RevisionRecord::class )
			->disableOriginalConstructor()
			->getMock();
		$revisionRecordMock
			->expects( $this->once() )
			->method( "getId" )
			->willReturn( 2 );
		$revisionLookupMock
			->expects( $this->once() )
			->method( "getRevisionByPageId" )
			->with( $expectedPageId )
			->willReturn( $revisionRecordMock );

		$helper = new SummaryHelper(
			$title, $authority, $pageStoreMock, $restHelperFactoryMock, $pagePropsMock, $revisionLookupMock, $titleFactoryMock
		);
		$revisionId = $helper->getRevisionId();
		$this->assertEquals( 2, $revisionId );
	}

	public function testGetRevisionIdThrowsException(): void {
		$title = "Test title";
		$authority = null;
		$pageStoreMock = $this->getMockBuilder( PageStore::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock = $this->getMockBuilder( PageRestHelperFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pagePropsMock = $this->getMockBuilder( PageProps::class )
			->disableOriginalConstructor()
			->getMock();
		$revisionLookupMock = $this->getMockBuilder( RevisionLookup::class )
			->disableOriginalConstructor()
			->getMock();
		$htmlHelperMock = $this->getMockBuilder( HtmlOutputRendererHelper::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock->expects( $this->once() )
			->method( "newHtmlOutputRendererHelper" )
			->willReturn( $htmlHelperMock );
		$pageMock = $this->getMockBuilder( ExistingPageRecord::class )
			->disableOriginalConstructor()
			->getMock();
		$titleFactoryMock = $this->getMockBuilder( TitleFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pageStoreMock->expects( $this->once() )
			->method( "getExistingPageByText" )
			->with( $title )
			->willReturn( $pageMock );

		$expectedPageId = 11;
		$pageMock->expects( $this->once() )
			->method( "getId" )
			->willReturn( $expectedPageId );
		$revisionLookupMock->expects( $this->once() )
			->method( "getRevisionByPageId" )
			->with( $expectedPageId )
			->willReturn( null );

		$helper = new SummaryHelper(
			$title, $authority, $pageStoreMock, $restHelperFactoryMock, $pagePropsMock, $revisionLookupMock, $titleFactoryMock
		);
		$this->expectException( \MediaWiki\Rest\HttpException::class );
		$helper->getRevisionId();
	}

	public function testShouldReturnEmptyExtractsTrue(): void {
		$title = "Test title";
		$authority = null;
		$pageStoreMock = $this->getMockBuilder( PageStore::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock = $this->getMockBuilder( PageRestHelperFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pagePropsMock = $this->getMockBuilder( PageProps::class )
			->disableOriginalConstructor()
			->getMock();
		$revisionLookupMock = $this->getMockBuilder( RevisionLookup::class )
			->disableOriginalConstructor()
			->getMock();
		$htmlHelperMock = $this->getMockBuilder( HtmlOutputRendererHelper::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock
			->expects( $this->once() )
			->method( "newHtmlOutputRendererHelper" )
			->willReturn( $htmlHelperMock );

		$pageMock = $this->getMockBuilder( ExistingPageRecord::class )
			->disableOriginalConstructor()
			->getMock();
		$titleFactoryMock = $this->getMockBuilder( TitleFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pageStoreMock
			->expects( $this->once() )
			->method( "getExistingPageByText" )
			->with( $title )
			->willReturn( $pageMock );

		$helper = new SummaryHelper(
			$title, $authority, $pageStoreMock, $restHelperFactoryMock, $pagePropsMock, $revisionLookupMock, $titleFactoryMock
		);

		$expectedPageId = 11;
		$pageMock->expects( $this->once() )
			->method( "getId" )
			->willReturn( $expectedPageId );
		$expectedNamespaceMain = 15;  // Assuming 15 is not in SUMMARY_NS_ALLOWLIST
		$pageMock->expects( $this->once() )
			->method( "getNamespace" )
			->willReturn( $expectedNamespaceMain );

		$titleMock = $this->getMockBuilder( Title::class )
			->disableOriginalConstructor()
			->getMock();
		$titleFactoryMock->expects( $this->once() )
			->method( "newFromID" )
			->with( $expectedPageId )
			->willReturn( $titleMock );
		$titleMock->expects( $this->once() )
			->method( "isWikitextPage" )
			->willReturn( true );
		$titleMock->expects( $this->once() )
			->method( "isMainPage" )
			->willReturn( true );
		$pageMock->expects( $this->once() )
			->method( "isRedirect" )
			->willReturn( true );

		$this->assertTrue( $helper->shouldReturnEmptyExtracts() );
	}

	public function testShouldReturnEmptyExtractsFalse(): void {
		$title = "Test title";
		$authority = null;
		$pageStoreMock = $this->getMockBuilder( PageStore::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock = $this->getMockBuilder( PageRestHelperFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pagePropsMock = $this->getMockBuilder( PageProps::class )
			->disableOriginalConstructor()
			->getMock();
		$revisionLookupMock = $this->getMockBuilder( RevisionLookup::class )
			->disableOriginalConstructor()
			->getMock();
		$htmlHelperMock = $this->getMockBuilder( HtmlOutputRendererHelper::class )
			->disableOriginalConstructor()
			->getMock();
		$restHelperFactoryMock
			->expects( $this->once() )
			->method( "newHtmlOutputRendererHelper" )
			->willReturn( $htmlHelperMock );

		$pageMock = $this->getMockBuilder( ExistingPageRecord::class )
			->disableOriginalConstructor()
			->getMock();
		$titleFactoryMock = $this->getMockBuilder( TitleFactory::class )
			->disableOriginalConstructor()
			->getMock();
		$pageStoreMock
			->expects( $this->once() )
			->method( "getExistingPageByText" )
			->with( $title )
			->willReturn( $pageMock );

		$helper = new SummaryHelper(
			$title, $authority, $pageStoreMock, $restHelperFactoryMock, $pagePropsMock, $revisionLookupMock, $titleFactoryMock
		);

		$expectedPageId = 11;
		$pageMock->expects( $this->once() )
			->method( "getId" )
			->willReturn( $expectedPageId );
		$expectedNamespaceMain = 0;
		$pageMock->expects( $this->once() )
			->method( "getNamespace" )
			->willReturn( $expectedNamespaceMain );

		$titleMock = $this->getMockBuilder( Title::class )
			->disableOriginalConstructor()
			->getMock();
		$titleFactoryMock->expects( $this->once() )
			->method( "newFromID" )
			->with( $expectedPageId )
			->willReturn( $titleMock );
		$titleMock->expects( $this->once() )
			->method( "isWikitextPage" )
			->willReturn( false );
		$titleMock->expects( $this->once() )
			->method( "isMainPage" )
			->willReturn( false );
		$pageMock->expects( $this->once() )
			->method( "isRedirect" )
			->willReturn( false );

		$this->assertFalse( $helper->shouldReturnEmptyExtracts() );
	}
}
