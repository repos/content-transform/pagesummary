<?php
use MediaWiki\Extension\PageSummary\SummaryOutput;
use PHPUnit\Framework\TestCase;

final class SummaryOutputTest extends TestCase {
	/**
	 * @covers MediaWiki\Extension\PageSummary\SummaryOutput::generate
	 */
	public function testSummaryOutput(): void {
		$title = "Main_Page";
		$html = "<html><body><div>Example</div></body></html>";
		$pageProps = [ [ "hello" ], [ "world" ] ];
		$pageImages = [
			"3" => [
				"pageid" => 3,
				"ns" => 0,
				"title" => "Test title"
			]
		];
		$revisionId = 11;
		$pageCoordinates = [
			"lat" => 37,
			"lon" => -122
		];
		$shouldReturnEmptyExtracts = false;
		$output = new SummaryOutput( $title, $html, $pageProps, $pageImages, $revisionId, $pageCoordinates, $shouldReturnEmptyExtracts );
		$this->assertEquals( $output->title, $title );
		$this->assertEquals( [
			"description" => "Description for page Main_Page",
			"pageProps" => $pageProps,
			"pageImages" => $pageImages,
			"revisionId" => $revisionId,
			"pageCoordinates" => $pageCoordinates,
			"shouldReturnEmptyExtracts" => $shouldReturnEmptyExtracts
		],
			$output->generate()
		);
	}

	/**
	 * @covers MediaWiki\Extension\PageSummary\SummaryOutput::createDocumentFromLeadSection
	 */
	public function testCreateDocumentFromLeadSection(): void {
		$html = <<<HTML
		<!DOCTYPE html>
		<html>
		<head>
			<title>Test Page</title>
		</head>
		<body>
			<section data-mw-section-id="0">
				<p>Here is the lead section.</p>
			</section>
			<section data-mw-section-id="1">
				<p>Here is another section.</p>
			</section>
			<section data-mw-section-id="2">
				<p>Here is one more section.</p>
			</section>
		</body>
		</html>         
		HTML;
		$title = "Main_Page";
		$pageProps = [ [ "hello" ], [ "world" ] ];
		$pageImages = [
			"3" => [
				"pageid" => 3,
				"ns" => 0,
				"title" => "Test title"
			]
		];
		$revisionId = 11;
		$pageCoordinates = [
			"lat" => 37,
			"lon" => -122
		];
		$shouldReturnEmptyExtracts = false;
		$output = new SummaryOutput( $title, $html, $pageProps, $pageImages, $revisionId, $pageCoordinates, $shouldReturnEmptyExtracts );

		$document = $output->createDocumentFromLeadSection( $html );
		$divElement = $document->getElementsByTagName( "div" )->item( 0 );
		$this->assertNotNull( $divElement, "Lead section div not found" );
		$pElements = $divElement->getElementsByTagName( "p" );
		$this->assertCount( 1, $pElements, "Lead section does not contain exactly one <p> element" );
		$textContent = $pElements->item( 0 )->textContent;
		$this->assertEquals( "Here is the lead section.", $textContent );
	}
}
