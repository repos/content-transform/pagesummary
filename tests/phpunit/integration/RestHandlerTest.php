<?php

use MediaWiki\Extension\PageSummary\RestHandler;
use MediaWiki\Rest\RequestData;

/**
 * @covers MediaWiki\Extension\PageSummary\RestHandler
 * @group Database
 */
class RestHandlerTest extends MediaWikiIntegrationTestCase {
	use MediaWiki\Tests\Rest\Handler\HandlerTestTrait;

	public function testGetMissingTitle() {
		$handler = new RestHandler();
		$page = $this->getNonexistingTestPage( "NonExistingPage" );
		$request = new RequestData(
			[ "pathParams" => [ "title" => $page->getTitle()->getPrefixedText() ] ]
		);
		$this->initHandler( $handler, $request );
		$this->expectException( MediaWiki\Rest\HttpException::class );
		$handler->run( "NonExistingPage" );
	}

	public function testGetExistingTitle() {
		$handler = new RestHandler();
		$page = $this->getExistingTestPage( "ExistingPage" );
		$request = new RequestData(
			[ "pathParams" => [ "title" => $page->getTitle()->getPrefixedText() ] ]
		);
		$res = $this->executeHandler( $handler, $request );
		$this->assertEquals( 200, $res->getStatusCode() );
		$obj = FormatJson::parse( $res->getBody()->getContents() );
		$this->assertEquals( "Description for page ExistingPage", $obj->value->description );
	}
}
